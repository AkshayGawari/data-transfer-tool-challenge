# Data Transfer Tool Challenge #

We would like you to create a Console application that can be used to transfer data to and from CSV files. The app should be able to be scheduled so the ability to run the app without the need for manual option selection is crucial.

## Getting started ##
You can either fork this repo and create a pull request once finished with your name as the title or download the keys and submit your code via a zip file. 

Don't spend too long on this challenge, if you feel you have spent too much time finish off and add comments on what you would do next. 

## Export ##

Tha app should dynamically read data from a data source and transform it into a csv file. 

Currently the data source we are using is a MS SQL Server database tables however we would like to be able to introduce new sources with minimal amounts of code changes so your app should take this into consideration. 

We also need the ability to chose weather or not to encrypt the file, We are currently using PGP encryption (keys in repo) but may wish to add other options in the future with minimal amounts of code changes so you app should take this into consideration. 


## Import ##

As well as exporting data we also want the ability to use this app to import data into a SQL database. 

We want to be able to reuse this app for any csv file and have minimal work on the database to set this up for a new file template. 

By default the csv will have a ‘,’ delimiter however we would like the ability to specify what delimitator to is being used. 

Some of the csv’s may be encrypted so we also need the ability to decrypt them. For the purposes of this challenge use the same pgp keys as for the export challenge.  


## PGP Keys ##
Both the public and private keys can be found in the "Keys" folder of the repo. the passphrase used is ROITestKey123!
